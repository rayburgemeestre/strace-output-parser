#include <iostream>
#include <sstream>
#include <cstdio>

#define CURLOPT_VERBOSE_VALUE 0

#define PICOJSON_USE_INT64
#include "picojson.h"

#include "metadataparser.hpp"

#include "Printer.H"
#include "Absyn.H"

using namespace std;

#include "Printer.H"
#include "Absyn.H"
#include "Parser.H"

int test()
{
    string line;
    int returnValue = 0;

    while (getline(cin, line)) {

        StraceLines *parse_tree = pStraceLines(line.c_str());

        if (parse_tree) {
            printf("\nParse Succesful!\n");
            printf("\n[Abstract Syntax]\n");
            ShowAbsyn *s = new ShowAbsyn();
            printf("%s\n\n", s->show(parse_tree));
            printf("[Linearized Tree]\n");
            PrintAbsyn *p = new PrintAbsyn();
            printf("%s\n\n", p->print(parse_tree));
        }
        else {
            returnValue = 1;
        }
    }

    return returnValue;
}

#include <curl/curl.h>

void recreate_elasticsearch_index(string baseurl, CURL *curl)
{
    cout << "[DELETE strace_index from ElasticSearch]" << endl << " <<< ";
    curl_easy_setopt(curl, CURLOPT_VERBOSE, CURLOPT_VERBOSE_VALUE);
    curl_easy_setopt(curl, CURLOPT_URL, baseurl.c_str());
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(""));
    CURLcode res = curl_easy_perform(curl);
    cout << endl << endl;

    cout << "[PUT strace_index in ElasticSearch]" << endl << " <<< ";
    //string mapping = "{\"mappings\": { \"@timestamp\": { \"type\" : \"date\" } } }";
    string mapping = "";
    curl_easy_setopt(curl, CURLOPT_VERBOSE, CURLOPT_VERBOSE_VALUE);
    curl_easy_setopt(curl, CURLOPT_URL, baseurl.c_str());
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, mapping.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, mapping.size());
    res = curl_easy_perform(curl);
    cout << endl << endl;
}

void store_in_elasticsearch(string baseurl, CURL *curl, std::map<string, picojson::value> &metaData)
{
    cout << "[POST strace_line to strace_index in ElasticSearch]" << endl;
    cout << " >>> " << picojson::value(metaData).serialize(false) << endl;
    cout << " <<< ";
    string url(baseurl + "strace_line/");
    url.append(string(metaData["_id"].get<string>()));
    // ElasticSearch 2 doesn't seem to like having _id in the json payload as well
    metaData.erase("_id");

    curl_easy_setopt(curl, CURLOPT_VERBOSE, CURLOPT_VERBOSE_VALUE);
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    string postData(picojson::value(metaData).serialize(false));
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, postData.size());
    CURLcode res = curl_easy_perform(curl);
    cout << endl << endl;
}

int es(string host, string port, string index)
{
    string line;
    int returnValue = 0;
    std::map<int64_t, string> streamThreadMapping;

    string lastOccuredThread(MetaDataParser::generate_uuid());

    string baseurl("http://" + host + ":" + port + "/" + index + "/");

    CURL *curl = curl_easy_init();

    recreate_elasticsearch_index(baseurl, curl);

    string requestUuid = "0739b3a0-830f-4899-8210-fead2649ef74";//MetaDataParser::generate_uuid();

    long long sequenceNumber = 1;

    while (getline(cin, line)) {

        cout << "[parsing line from stdin]" << endl;
        cout << " line = " << line << endl;

        std::map<string, picojson::value> metaData;
        MetaDataParser parser(metaData, streamThreadMapping, lastOccuredThread);

        StraceLines *parse_tree = pStraceLines(line.c_str());

        metaData["@sequence"]                = picojson::value((int64_t)sequenceNumber++);
        metaData["request_id"]               = picojson::value(requestUuid);
        metaData["raw_line"]                 = picojson::value(line);
        metaData["parse_ok"]                 = picojson::value(!!parse_tree);
        metaData["occured_during_thread_id"] = picojson::value(lastOccuredThread);

        cout << " parse_ok  = " << (parse_tree ? "TRUE" : "FALSE") << endl;

        if (!parse_tree) {
            returnValue = 1;
            continue;
        }

        parser.visit(parse_tree);
        ShowAbsyn s;
        cout << " ast  = " << s.show(parse_tree) << endl;

        // cout << picojson::value(metaData).serialize(true) << endl;

        cout << endl;

        store_in_elasticsearch(baseurl, curl, metaData);
    }

    curl_easy_cleanup(curl);
    return returnValue;
}

int main(int argc, char ** argv)
{
    string mode = (argc >= 2) ? string(argv[1]) : "default";
    string host = (argc >= 3) ? string(argv[2]) : "localhost";
    string port = (argc >= 4) ? string(argv[3]) : "9200";
    string index = (argc >= 4) ? string(argv[4]) : "strace_index";

    if (mode == "elasticsearch")
        return es(host, port, index);

    else if (mode == "default")
        return test();

    cerr << "Usage: strace-output-parser [ \"default\" | \"elasticsearch\" <host> <port> <index_name> ]" << endl;

    return -1;
}

