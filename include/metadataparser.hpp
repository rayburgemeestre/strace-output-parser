#pragma once

#include <vector>
#include <map>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

using namespace boost::uuids;
using namespace std;

#include "Parser.H"
#include "Skeleton.H"
#include "Printer.H"

string ReplaceAll(string str, const string& from, const string& to)
{
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

/**
 * Class MetaDataParser walks the strace output syntax tree and collects some meta data
 */
class MetaDataParser : public Skeleton
{
private:
    bool stream_matched_;
    bool first_param_not_int_;

    string function_;
    string timestamp_;

    int function_num_ ;
    int list_param_level_ ;

    std::map<string, picojson::value> &metadata_;
    std::map<int64_t, string> &stream_thread_mapping_;
    string &last_occured_thread_;

public:

    /**
     * Constructor
     */
    MetaDataParser(std::map<string, picojson::value> &metaDataBucket,
                   std::map<int64_t, string> &streamThreadMapping,
                   string &lastOccuredThread) 
        : stream_matched_(false),
          first_param_not_int_(false),
          function_num_(0),
          list_param_level_(0),
          metadata_(metaDataBucket),
          stream_thread_mapping_(streamThreadMapping),
          last_occured_thread_(lastOccuredThread)
    {}

    void visit(Visitable* v)
    {
        list_param_level_ = 0;
        function_num_ = 0;
        v->accept(this);
    }

    static string generate_uuid()
    {
        uuid uuid = boost::uuids::random_generator()();
        stringstream ss;
        ss << uuid;
        return ss.str();
    }

    void visitEStraceLine(EStraceLine* p)
    {
        function_num_ = 0;

        Skeleton::visitEStraceLine(p);
    }

    void visitEPidNumber(EPidNumber *p)
    {
        metadata_["pid_number"] = picojson::value(static_cast<int64_t>(p->integer_));
        Skeleton::visitEPidNumber(p);
    }

    void visitEFunction(EFunction* p)
    {
        function_num_++;

        function_       = p->ident_;

        metadata_["function"]  = picojson::value(function_);

        // Initialize with false, or immediately set to true for some specific functions
        stream_matched_ = (function_ == "set_tid_address" /*|| function_ == "clone"*/);
        first_param_not_int_ = false;

        Skeleton::visitEFunction(p);
    }

    void visitListParam(ListParam *p)
    {
        list_param_level_++;

        if ((*p).empty() || !dynamic_cast<EParamInteger *>((*p)[0]) || first_param_not_int_) {
            // Set a flag that we couldn't find a stream as the first parameter. 
            // i.e.: "access("/etc/ld.so.nohwcap", F_OK) = -1 ENOENT (No such file or directory)"
            //       does not work on a stream, that pattern seems to be read(stream, ..). 
            //       (side note: we also conclude it doesn't create a stream due to the negative integer)
            first_param_not_int_ = true;
            Skeleton::visitListParam(p);
            list_param_level_--;
            return;
        }

        auto param          = dynamic_cast<EParamInteger *>((*p)[0]);
        auto positiveNumber = static_cast<EPositiveNumber *>(param->number_);

        // A "function" element can be encountered as parameters etc., more levels away in the tree
        //  we are only interested in the highest level (i.e., read(3, ..)/write(3, ..)'s first parameter.)
        if (list_param_level_ == 1) {

            metadata_["stream"]            = picojson::value((int64_t)positiveNumber->integer_);
            metadata_["stream_state_uses"] = picojson::value(true);
            metadata_["thread_id"]         = picojson::value(getThreadIdForStream((int64_t)positiveNumber->integer_));

            // Prevent this value from being overwritten by the return value.
            // i.o.w.: "read(3, ...) = 100", the 100 should not be mistaken for a created stream.
            //      in "open(...)    = 3" the "return" *is* the created stream.
            stream_matched_ = true;
        }

        Skeleton::visitListParam(p);

        list_param_level_--;
    }

    string paramAsString(const Param * const parameter) const
    {
        /*
        if (auto *c = dynamic_cast<const EParamIdent * const>(parameter))
            return c->ident_;

        if (auto *c = dynamic_cast<const EParamString * const>(parameter))
            return c->string_;
        */

        // Fallback to the default generated printer for all cases..
        PrintAbsyn printer;
        string s(printer.print(const_cast<Param *>(parameter)));
        return trim(s);
    }


    void visitEParamKeyValue(EParamKeyValue *p)
    {
        EParamKeyValue &keyval(*p);

        string key = paramAsString(keyval.param_1);
        string value = paramAsString(keyval.param_2);

        if (metadata_.find("extracted_kv_pairs") == std::end(metadata_))
            metadata_["extracted_kv_pairs"] = picojson::value(map<string, picojson::value>());

        (metadata_["extracted_kv_pairs"].get<picojson::object>())[key] = picojson::value(value);

        Skeleton::visitEParamKeyValue(p);
    }

    void visitERetvalNumber(ERetvalNumber* p)
    {
        // In case Return value number is of type "RetvalPositiveNumber", it's a positive integer.
        //  hence it might be a stream that was created by the called function.
        auto si = dynamic_cast<EPositiveNumber *>(p->number_);

        // In order for us to think it's a created stream we need to be sure we didn't already
        //  conclude the function already works with a stream (i.e. read(3, ..) = 100).
        if (stream_matched_ || !si)
            return Skeleton::visitERetvalNumber(p);
        
        // Also the value should be >= 3, because otherwise it's the already existant 
        //  stdin/stdout/stderr. We already excluded negative values, as those are parsed as
        //  a ESignedIntNegative object.
        int fd = si->integer_;
        if (fd <= 2 /* MAX(stdin, stdout, stderr) */)
            return Skeleton::visitERetvalNumber(p);

        // A simple check if this corresponds to the first encountered function. 
        // Otherwise if we encounter a function as a parameter deeper in the parsetree.
        if (function_num_ == 1) {
            // Generate unique uuid for this "thread" (communication on specific filedescriptor)
            string uuidstr(generate_uuid());

            if (function_ == "clone") {
                metadata_["return"] = picojson::value((int64_t)fd);
            }
            else {
                metadata_["stream"]                   = picojson::value((int64_t)fd);
                metadata_["stream_state_creates"]     = picojson::value(true);
                metadata_["thread_id"]                = picojson::value(uuidstr);
                metadata_["occured_during_thread_id"] = picojson::value(uuidstr);
            }

            registerStream((int64_t)fd, uuidstr);
        }

        return Skeleton::visitERetvalNumber(p);
    }

    void visitEEpochElapsedTime(EEpochElapsedTime *p)
    {
        auto secs      = static_cast<ESeconds *>(p->seconds_);
        auto microsecs = static_cast<EMicroseconds *>(p->microseconds_);

        stringstream ss;
        ss << secs->integer_ 
           << "." 
           << microsecs->integer_;
        timestamp_ = ss.str();

        metadata_["_id"]       = picojson::value(timestamp_);
        metadata_["timestamp"] = picojson::value(atof(timestamp_.c_str())); 

        // Prepare datetime in a format that ElasticSearch understands out of the box
        time_t     now = static_cast<time_t>(secs->integer_);//.429679;
        struct tm  ts  = *gmtime(&now);
        char       buf[80] = {0x00};
        strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%S.", &ts);
        sprintf(buf, "%s%03.0fZ", buf, static_cast<double>(microsecs->integer_) / 1000.0);

        metadata_["@timestamp"] = picojson::value(buf);

        Skeleton::visitEEpochElapsedTime(p);
    }

    void visitString(String x)
    {
        // The generated BNF parser, seemed to have replaced \r\n with r\n :-)
        // Let's just pretend that never happened...
        x = ReplaceAll(x, "r\n", "\n");

        if (metadata_.find("strings") == std::end(metadata_))
            metadata_["strings"] = picojson::value(vector<picojson::value>());

        metadata_["strings"].get<picojson::array>().push_back(picojson::value(x));

        Skeleton::visitString(x);
    }

private:

    void registerStream(int64_t fd, string uuidstr)
    {
        stream_thread_mapping_[fd] = uuidstr;
        last_occured_thread_ = uuidstr;
    }

    string getThreadIdForStream(int64_t fd)
    {
        if (fd == 0)
            return "*** stdin ***";
        if (fd == 1)
            return "*** stdout ***";
        if (fd == 2)
            return "*** stderr ***";

        auto pos = stream_thread_mapping_.find(fd);
        if (pos == stream_thread_mapping_.end()) {
            return "*** unregistered ***";
        }

        return pos->second;
    }
};
