#!/bin/ksh93

# strace -fF -ttt -o out.txt -s 80 curl http://cppse.nl/test.txt
# note that -o produces slightyl different output


bnfc -m -cpp_stl ${1}.cf && make -j 8 Test$1 && \
cat /tmp/test | ./Test$1
