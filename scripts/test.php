<?php
$lines = explode("\n", trim(file_get_contents('test3.txt')));
foreach ($lines as $line) {
    $line = trim($line);
    if (file_put_contents('/tmp/test', $line . "\n")) {
        printf("processing line: " . $line . "\n");
        if ('line 1' === system('cat /tmp/test | ./TestFILE')) 
            // If a line fails, test with: make clean && ksh make.ksh FILE
            exit(1);
    }
}
?>
